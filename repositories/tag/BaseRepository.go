package tag

import "sajgo/models"

type BaseRepository interface {

	// ExistByName checks if there is a tag with the same name
	ExistByName(name string) (bool, error)

	// Add Add a Tag
	Add() error

	// Get gets a list of tags based on paging and constraints
	Get(pageNum int, pageSize int, maps interface{}) ([]models.Tag, error)

	// GetTotal counts the total number of tags based on the constraint
	GetTotal(maps interface{}) (int, error)

	// ExistById determines whether a Tag exists based on the ID
	ExistById(id int) (bool, error)

	// Delete delete a tag
	Delete(id int) error

	// Edit modify a single tag
	Edit(id int, data interface{}) error

}
