package tag

import "sajgo/models"

type Repository struct {
	model *models.Tag
}

//return new instance model
func GetInstance()*models.Tag{
	return &models.Tag{}
}
//return new instance TagRepository
func GetRepository(tag *models.Tag)*Repository {
	return &Repository{
		model: tag,
	}
}

// Add Add a Tag
func (repository *Repository)Add() error  {
	return repository.model.Add()
}
// Add Add a Tag
func (repository *Repository)Delete(id int) error  {
	return repository.model.Delete(id)
}

// ExistByName checks if there is a tag with the same name
func (repository *Repository)ExistByName(name string) (bool, error) {
	   return repository.model.ExistByName(name)
}
// ExistByName checks if there is a tag with the same name
func (repository *Repository)ExistById(id int) (bool, error) {
	return repository.model.ExistTagById(id)
}

// Add Add a Tag
func (repository *Repository)Get(pageNum int, pageSize int, maps interface{}) ([]models.Tag, error)  {
	return repository.model.GetTags(pageNum, pageSize, maps)
}

// GetTotal counts the total number of tags based on the constraint
func (repository Repository) GetTotal(maps interface{})(int, error)  {
	return repository.model.GetTagTotal(maps)
}

// Edit modify a single tag
func (repository Repository) Edit(id int, data interface{}) error{
	return repository.model.Edit(id,data)
}

