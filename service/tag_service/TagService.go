package tag_service

import (
	"encoding/json"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/tealeg/xlsx"
	"io"
	"sajgo/models"
	"sajgo/pkg/export"
	"sajgo/pkg/file"
	"sajgo/pkg/gredis"
	"sajgo/pkg/logging"
	"sajgo/pkg/setting"
	"sajgo/repositories/tag"
	"sajgo/service/cache_service"
	"strconv"
	"time"
)

type Tag struct {
	ID         int
	Name       string
	CreatedBy  string
	ModifiedBy string
	State      int

	PageNum  int
	PageSize int
}

type Service struct {
	Repository tag.BaseRepository
	PageNum    int
	PageSize   int
}

// ExistByName check exist tag by Name
func (service *Service) ExistByName(name string) (bool, error) {
	return service.Repository.ExistByName(name)
}

// Add Tag Service
func (service *Service) Add() error {
	return service.Repository.Add()
}

// GetAll  Get All Tag Service
func (service *Service) GetAll(t *models.Tag) ([]models.Tag, error) {
	var (
		tags, cacheTags []models.Tag
	)
	cache := cache_service.Tag{
		State: t.State,

		PageNum:  service.PageNum ,
		PageSize: service.PageSize,
	}
	key := cache.GetTagsKey()

	if gredis.Exist(key) {
		data, err := gredis.Get(key)
		if err != nil {
			logging.Info(err)
		} else {
			json.Unmarshal(data, &cacheTags)
			return cacheTags, nil
		}
	}

	tags, err := service.Repository.Get(service.PageNum, service.PageSize, t.GetMaps())

	fmt.Println(tags)

	if err != nil {
		return nil, err
	}

	gredis.Set(key, tags, setting.AppSetting.RedisCacheTime)

	return tags, nil

}

// Count Tag Count Service
func (service *Service) Count(maps interface{}) (int, error) {
	return service.Repository.GetTotal(maps)
}


// ExistById check exist tag by id
func (service *Service) ExistById(id int) (bool, error) {
	return service.Repository.ExistById(id)
}


// Edit Tag Service
func (service *Service) Edit(t *models.Tag) error {
	data := make(map[string]interface{})
	data["modified_by"] = t.ModifiedBy
	data["name"] = t.Name
	if t.State > 0 {
		data["state"] = t.State
	}
	return service.Repository.Edit(t.ID, data)
}

// Delete Tag Service
func (service *Service) Delete(id int) error {
	return service.Repository.Delete(id)
}




func (service *Service) Export(tag *models.Tag) (string, error) {
	tags, err := service.Repository.Get(service.PageNum,service.PageSize,tag.GetMaps())

	if err != nil {
		return "", err
	}

	xlsFile := xlsx.NewFile()
	sheet, err := xlsFile.AddSheet("Tags")
	if err != nil {
		return "", err
	}

	titles := []string{"ID", "Name", "CreatedBy", "Created_at", "ModifiedBy", "Updated_at"}
	row := sheet.AddRow()

	var cell *xlsx.Cell
	for _, title := range titles {
		cell = row.AddCell()
		cell.Value = title
	}

	for _, v := range tags {
		values := []string{
			strconv.Itoa(v.ID),
			v.Name,
			v.CreatedBy,
			strconv.Itoa(v.CreatedOn),
			v.ModifiedBy,
			strconv.Itoa(v.ModifiedOn),
		}

		row = sheet.AddRow()
		for _, value := range values {
			cell = row.AddCell()
			cell.Value = value
		}
	}

	time := strconv.Itoa(int(time.Now().Unix()))
	filename := "tags-" + time + export.EXT

	dirFullPath := export.GetExcelFullPath()
	err = file.IsNotExistMkDir(dirFullPath)
	if err != nil {
		return "", err
	}

	err = xlsFile.Save(dirFullPath + filename)
	if err != nil {
		return "", err
	}

	return filename, nil
}

func (service *Service) Import(r io.Reader) error {
	xlsx, err := excelize.OpenReader(r)
	if err != nil {
		return err
	}

	rows := xlsx.GetRows("Tags")
	for irow, row := range rows {
		if irow > 0 {
			var data []string
			for _, cell := range row {
				data = append(data, cell)
			}
			//models.AddTag(data[1], 1, data[2])
		}
	}

	return nil
}


