package routers

import (
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"net/http"
	"sajgo/controllers"
	v1 "sajgo/controllers/v1"
	"sajgo/middleware/jwt"
	"sajgo/pkg/export"
	"sajgo/pkg/qrcode"
	"sajgo/pkg/upload"
)

// InitRouter initialize routing information
func InitRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	r.StaticFS("/export", http.Dir(export.GetExcelFullPath()))
	r.StaticFS("/upload/images", http.Dir(upload.GetImageFullPath()))
	r.StaticFS("/qrcode", http.Dir(qrcode.GetQrCodeFullPath()))

	r.POST("/auth", controllers.GetAuth)
	r.POST("/upload", controllers.UploadImage)

	apiv1 := r.Group("/api/v1")



	apiv1.Use(jwt.JWT())
	{
		//get tags
		apiv1.GET("/tags", v1.GetTags)
		//add tag
		apiv1.POST("/tags", v1.AddTag)

		//edit tag
		apiv1.PUT("/tags/:id", v1.EditTag)
		//delete tag
		apiv1.DELETE("/tags/:id", v1.DeleteTag)

		//exist tags
		apiv1.GET("/tags/exist", v1.ExistTag)

		//export tags
		apiv1.POST("/tags/export", v1.ExportTag)

		//import tags
		apiv1.POST("/tags/import", v1.ImportTag)

	}
	return r
}
