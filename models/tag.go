package models

import (
	"github.com/jinzhu/gorm"
)

type Tag struct {
	Model
	Name       string `json:"name"`
	CreatedBy  string `json:"created_by"`
	ModifiedBy string `json:"modified_by"`
	State      int    `json:"state"`
}

// ExistTagByID determines whether a Tag exists based on the ID
func (t *Tag)ExistTagById(id int) (bool, error) {
	var tag Tag
	err := db.Select("id").Where("id = ? AND deleted_on = ?", id, 0).First(&tag).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}
	if tag.ID > 0 {
		return true, nil
	}
	return false, nil
}

// DeleteTag delete a tag
func (t *Tag)Delete(id int) error {
	if err := db.Where("id=?", id).Delete(&Tag{}).Error; err != nil {
		return err
	}
	return nil
}

// EditTag modify a single tag
func (t *Tag)Edit(id int, data interface{}) error {
	if err := db.Model(t).Where("id=? AND deleted_on=?", id, 0).Update(data).Error; err != nil {
		return err
	}
	return nil
}

// CleanAllTag clear all tag
func CleanAllTag() (bool, error) {
	if err := db.Unscoped().Where("deleted_on=?", 0).Delete(&Tag{}).Error; err != nil {
		return false, err
	}
	return true, nil
}

///

// GetTagTotal counts the total number of tags based on the constraint
func (tag *Tag)GetTagTotal(maps interface{}) (int, error) {
	var count int
	if err := db.Model(&Tag{}).Where(maps).Count(&count).Error; err != nil {
		return 0, err
	}
	return count, nil
}

// GetTags gets a list of tags based on paging and constraints
func (tag *Tag)GetTags(pageNum int, pageSize int, maps interface{}) ([]Tag, error) {
	var (
		tags []Tag
		err  error
	)

	if pageSize > 0 && pageNum > 0 {
		err = db.Where(maps).Find(&tags).Offset(pageNum).Limit(pageSize).Error
	} else {
		err = db.Where(maps).Find(&tags).Error
	}

	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}

	return tags, nil
}

// AddTag Add a Tag
func (tag *Tag)Add() error {
	if err := db.Create(&tag).Error; err != nil {
		return err
	}
	return nil
}


// ExistTagByName checks if there is a tag with the same name
func (tag *Tag)ExistByName(name string) (bool, error) {

	err := db.Select("id").Where("name = ? AND deleted_on = ? ", name, 0).First(tag).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}

	if tag.ID > 0 {
		return true, nil
	}

	return false, nil
}
// get key maps
func (t *Tag) GetMaps() map[string]interface{} {
	maps := make(map[string]interface{})
	maps["deleted_on"] = 0

	if t.Name != "" {
		maps["name"] = t.Name
	}
	if t.State >= 0 {
		maps["state"] = t.State
	}

	return maps
}
