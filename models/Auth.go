package models

import (
	"github.com/jinzhu/gorm"
	"sajgo/pkg/util"
)

type Auth struct {
	ID       int    `gorm:"primary_key" json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
}

// CheckAuth checks if authentication information exists
func CheckAuth(username, password string) (bool, error) {
	var auth Auth
	err := db.Select("id").Where(Auth{Username: username}).First(&auth).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}

	ok :=util.CheckPasswordHash(password, auth.Password)

	if !ok {
		return false, nil
	}

	if auth.ID > 0 {
		return true, nil
	}

	return false, nil
}