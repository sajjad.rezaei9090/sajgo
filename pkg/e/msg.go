package e

var MsgFlags = map[int]string{
	SUCCESS:                         "ok",
	ERROR:                           "fail",
	INVALID_PARAMS:                  "داده های ارسالی صحیح نمی باشد",
	ERROR_EXIST_TAG:                 "تگ ارسالی تکراری می باشد",
	ERROR_EXIST_TAG_FAIL:            "EXIST_TAG_FAIL",
	ERROR_NOT_EXIST_TAG:             "NOT_EXIST_TAG",
	ERROR_GET_TAGS_FAIL:             "GET_TAGS_FAIL",
	ERROR_COUNT_TAG_FAIL:            "COUNT_TAG_FAIL",
	ERROR_ADD_TAG_FAIL:              "ADD_TAG_FAIL",
	ERROR_EDIT_TAG_FAIL:             "EDIT_TAG_FAIL",
	ERROR_DELETE_TAG_FAIL:           "DELETE_TAG_FAIL",
	ERROR_EXPORT_TAG_FAIL:           "EXPORT_TAG_FAIL",
	ERROR_IMPORT_TAG_FAIL:           "IMPORT_TAG_FAIL",
	ERROR_NOT_EXIST_ARTICLE:         "NOT_EXIST_ARTICLE",
	ERROR_ADD_ARTICLE_FAIL:          "ADD_ARTICLE_FAI",
	ERROR_DELETE_ARTICLE_FAIL:       "DELETE_ARTICLE_FAIL",
	ERROR_CHECK_EXIST_ARTICLE_FAIL:  "CHECK_EXIST_ARTICLE_FAIL",
	ERROR_EDIT_ARTICLE_FAIL:         "EDIT_ARTICLE_FAIL",
	ERROR_COUNT_ARTICLE_FAIL:        "COUNT_ARTICLE_FAIL",
	ERROR_GET_ARTICLES_FAIL:         "GET_ARTICLES_FAIL",
	ERROR_GET_ARTICLE_FAIL:          "GET_ARTICLE_FAIL",
	ERROR_GEN_ARTICLE_POSTER_FAIL:   "GEN_ARTICLE_POSTER_FAI",
	ERROR_AUTH_CHECK_TOKEN_FAIL:     "AUTH_CHECK_TOKEN_FAIL",
	ERROR_AUTH_CHECK_TOKEN_TIMEOUT:  "AUTH_CHECK_TOKEN_TIMEOUT",
	ERROR_AUTH_TOKEN:                "AUTH_TOKEN",
	ERROR_AUTH:                      "توکن منقضی شده است",
	ERROR_UPLOAD_SAVE_IMAGE_FAIL:    "UPLOAD_SAVE_IMAGE_FAIL",
	ERROR_UPLOAD_CHECK_IMAGE_FAIL:   "_UPLOAD_CHECK_IMAGE_FAIL",
	ERROR_UPLOAD_CHECK_IMAGE_FORMAT: "UPLOAD_CHECK_IMAGE_FORMAT",
}

//GetMsg get error information based on Code
func GetMsg(code int)string  {
	msg,ok:=MsgFlags[code]
	if ok{
		return msg
	}
	return MsgFlags[ERROR]
}
